//! Length quantities for units used for astronomical distances.
//!
//! This group is mostly for fun, but still provides potentially useful quantities.
//!
//! * AstronomicalUnit - average distance between the sun and the earth
//! * LightSecond - distance light travels in a second
//! * LightMinute - distance light travels in a minute
//! * LightHour - distance light travels in a hour
//! * LightDay - distance light travels in a day
//! * LightWeek - distance light travels in a week
//! * LightMonth - distance light travels in a standard month (30 days)
//! * LightYear - distance light travels in a year (Julian year: 365.25 days)
//! * Parsec - one parallax second (approximately 3.26 light-years)
//!
//! # Examples
//!
//! ```rust
//! use quantities::length::{astronomical::{LightDay, LightYear}, si::Kilometer};
//! let alpha_centauri = LightYear::new(4.2);
//! let alpha_centauri_km: Kilometer = alpha_centauri.into();
//! println!("Distance in kilometers: {}", alpha_centauri_km);
//!
//! let dist = LightYear::new(1.0);
//! let dist2 = LightDay::new(365.25);
//! println!("{} + {} = {}", &dist, &dist2, dist + dist2);
//! ```

use crate::length::imperial;
use crate::length::si;
use crate::length::convert::*;

use quantity_macro::quantity;

const METERS_PER_LIGHT_SECOND: f64 = 299_792_458.0;
const KILOMETERS_PER_LIGHT_SECOND: f64 = METERS_PER_LIGHT_SECOND / 1000.0;
const METERS_PER_LIGHT_YEAR: f64 = 9_946_073_047_258_080.0;
const KILOMETERS_PER_LIGHT_YEAR: f64 = METERS_PER_LIGHT_YEAR / 1000.0;
const ASTRONOMICAL_UNITS_PER_LIGHT_YEAR: f64 = 63_240.87;
const KILOMETERS_PER_ASTRONOMICAL_UNIT: f64 = 1.495_979e+8;
const MILES_PER_ASTRONOMICAL_UNIT: f64 = 9.295_581e+7;
const PARSECS_PER_LIGHT_YEAR: f64 = 3.261_598;
const SECONDS_PER_MINUTE: f64 = 60.0;
const MINUTES_PER_HOUR: f64 = 60.0;
const SECONDS_PER_HOUR: f64 = 3600.0;
const SECONDS_PER_DAY: f64 = 86_400.0;
const MINUTES_PER_DAY: f64 = 1440.0;
const HOURS_PER_DAY: f64 = 24.0;
const DAYS_PER_WEEK: f64 = 7.0;
const DAYS_PER_MONTH: f64 = 30.0; // Days in a month are defined to 30 for units
const DAYS_PER_JULIAN_YEAR: f64 = 365.25;  // Light-years are defined as Julian years of 86400 second days

/// Distance light travels in a year (Julian year: 365.25 days)
#[derive(Debug, Default, Clone, Copy)]
#[quantity(units = "ly", alt = "light-years")]
pub struct LightYear(f64);

impl LightYear {
    pub fn new(value: f64) -> Self { Self(value) }
    pub fn get(&self) -> f64 { self.0 }

    pub fn from_kilometers(km: f64) -> Self { Self(km / KILOMETERS_PER_LIGHT_YEAR) }
    pub fn from_miles(miles: f64) -> Self { Self::from_kilometers(miles * KILOMETERS_PER_MILE) }
    pub fn from_au(au: f64) -> Self { Self::new(au / ASTRONOMICAL_UNITS_PER_LIGHT_YEAR) }
    pub fn from_parsecs(parsecs: f64) -> Self { Self::new(parsecs / PARSECS_PER_LIGHT_YEAR) }
}

// From trait
impl From<si::Kilometer> for LightYear {
    fn from(value: si::Kilometer) -> Self { Self::from_kilometers(value.get()) }
}
impl From<imperial::Mile> for LightYear {
    fn from(value: imperial::Mile) -> Self { Self::from_miles(value.get()) }
}
impl From<AstronomicalUnit> for LightYear {
    fn from(value: AstronomicalUnit) -> Self { Self::from_au(value.get()) }
}
impl From<Parsec> for LightYear {
    fn from(value: Parsec) -> Self { Self::from_parsecs(value.get()) }
}
impl From<LightDay> for LightYear {
    fn from(value: LightDay) -> Self { Self::new(value.get() / DAYS_PER_JULIAN_YEAR) }
}
impl From<LightWeek> for LightYear {
    fn from(value: LightWeek) -> Self { Self::new(value.get() / DAYS_PER_WEEK / DAYS_PER_JULIAN_YEAR) }
}
impl From<LightMonth> for LightYear {
    fn from(value: LightMonth) -> Self { Self::new(value.get() / DAYS_PER_MONTH / DAYS_PER_JULIAN_YEAR) }
}
impl From<LightYear> for si::Kilometer {
    fn from(value: LightYear) -> Self { Self::new(value.get() * KILOMETERS_PER_LIGHT_YEAR) }
}

/// Distance light travels in a second
#[derive(Debug, Default, Clone, Copy)]
#[quantity(units = "ls", alt = "light-seconds")]
pub struct LightSecond(f64);

impl LightSecond {
    pub fn new(value: f64) -> Self { Self(value) }
    pub fn get(&self) -> f64 { self.0 }

    pub fn from_kilometers(km: f64) -> Self { Self(km / KILOMETERS_PER_LIGHT_SECOND) }
    pub fn from_miles(miles: f64) -> Self { Self::from_kilometers(miles * KILOMETERS_PER_MILE) }
    pub fn from_au(au: f64) -> Self { Self::from_miles(au * MILES_PER_ASTRONOMICAL_UNIT) }
}

// From trait
impl From<si::Kilometer> for LightSecond {
    fn from(value: si::Kilometer) -> Self { Self::from_kilometers(value.get()) }
}
impl From<imperial::Mile> for LightSecond {
    fn from(value: imperial::Mile) -> Self { Self::from_miles(value.get()) }
}
impl From<AstronomicalUnit> for LightSecond {
    fn from(value: AstronomicalUnit) -> Self { Self::from_au(value.get()) }
}
impl From<LightMinute> for LightSecond {
    fn from(value: LightMinute) -> Self { Self::new(value.get() * SECONDS_PER_MINUTE) }
}
impl From<LightHour> for LightSecond {
    fn from(value: LightHour) -> Self { Self::new(value.get() * SECONDS_PER_HOUR) }
}
impl From<LightDay> for LightSecond {
    fn from(value: LightDay) -> Self { Self::new(value.get() * SECONDS_PER_DAY) }
}
impl From<LightSecond> for si::Kilometer {
    fn from(value: LightSecond) -> Self { Self::new(value.get() * KILOMETERS_PER_LIGHT_SECOND) }
}

/// Distance light travels in a minute
#[derive(Debug, Default, Clone, Copy)]
#[quantity(units = "lmin", alt = "light-minutes")]
pub struct LightMinute(f64);

impl LightMinute {
    pub fn new(value: f64) -> Self { Self(value) }
    pub fn get(&self) -> f64 { self.0 }

    pub fn from_kilometers(km: f64) -> Self { Self(km / KILOMETERS_PER_LIGHT_SECOND / SECONDS_PER_MINUTE) }
    pub fn from_miles(miles: f64) -> Self { Self::from_kilometers(miles * KILOMETERS_PER_MILE) }
    pub fn from_au(au: f64) -> Self { Self::from_kilometers(au * KILOMETERS_PER_ASTRONOMICAL_UNIT) }
}

// From trait
impl From<si::Kilometer> for LightMinute {
    fn from(value: si::Kilometer) -> Self { Self::from_kilometers(value.get()) }
}
impl From<imperial::Mile> for LightMinute {
    fn from(value: imperial::Mile) -> Self { Self::from_miles(value.get()) }
}
impl From<AstronomicalUnit> for LightMinute {
    fn from(value: AstronomicalUnit) -> Self { Self::from_au(value.get()) }
}
impl From<LightSecond> for LightMinute {
    fn from(value: LightSecond) -> Self { Self::new(value.get() / SECONDS_PER_MINUTE) }
}
impl From<LightHour> for LightMinute {
    fn from(value: LightHour) -> Self { Self::new(value.get() * MINUTES_PER_HOUR) }
}
impl From<LightDay> for LightMinute {
    fn from(value: LightDay) -> Self { Self::new(value.get() * MINUTES_PER_DAY) }
}
impl From<LightMinute> for si::Kilometer {
    fn from(value: LightMinute) -> Self { Self::new(value.get() * KILOMETERS_PER_LIGHT_SECOND * SECONDS_PER_MINUTE) }
}

/// Distance light travels in a hour
#[derive(Debug, Default, Clone, Copy)]
#[quantity(units = "lhr", alt = "light-hours")]
pub struct LightHour(f64);

impl LightHour {
    pub fn new(value: f64) -> Self { Self(value) }
    pub fn get(&self) -> f64 { self.0 }

    pub fn from_kilometers(km: f64) -> Self { Self(km / KILOMETERS_PER_LIGHT_SECOND / SECONDS_PER_HOUR) }
    pub fn from_miles(miles: f64) -> Self { Self::from_kilometers(miles * KILOMETERS_PER_MILE) }
    pub fn from_au(au: f64) -> Self { Self::from_kilometers(au * KILOMETERS_PER_ASTRONOMICAL_UNIT) }
}

// From trait
impl From<si::Kilometer> for LightHour {
    fn from(value: si::Kilometer) -> Self { Self::from_kilometers(value.get()) }
}
impl From<imperial::Mile> for LightHour {
    fn from(value: imperial::Mile) -> Self { Self::from_miles(value.get()) }
}
impl From<AstronomicalUnit> for LightHour {
    fn from(value: AstronomicalUnit) -> Self { Self::from_au(value.get()) }
}
impl From<LightSecond> for LightHour {
    fn from(value: LightSecond) -> Self { Self::new(value.get() / SECONDS_PER_HOUR) }
}
impl From<LightMinute> for LightHour {
    fn from(value: LightMinute) -> Self { Self::new(value.get() / MINUTES_PER_HOUR) }
}
impl From<LightDay> for LightHour {
    fn from(value: LightDay) -> Self { Self::new(value.get() * HOURS_PER_DAY) }
}
impl From<LightHour> for si::Kilometer {
    fn from(value: LightHour) -> Self { Self::new(value.get() * KILOMETERS_PER_LIGHT_SECOND * SECONDS_PER_HOUR) }
}

/// Distance light travels in a day
#[derive(Debug, Default, Clone, Copy)]
#[quantity(units = "ld", alt = "light-days")]
pub struct LightDay(f64);

impl LightDay {
    pub fn new(value: f64) -> Self { Self(value) }
    pub fn get(&self) -> f64 { self.0 }

    pub fn from_kilometers(km: f64) -> Self { Self(km / KILOMETERS_PER_LIGHT_SECOND / SECONDS_PER_DAY) }
    pub fn from_miles(miles: f64) -> Self { Self::from_kilometers(miles * KILOMETERS_PER_MILE) }
    pub fn from_au(au: f64) -> Self { Self::from_kilometers(au * KILOMETERS_PER_ASTRONOMICAL_UNIT) }
}

// From trait
impl From<si::Kilometer> for LightDay {
    fn from(value: si::Kilometer) -> Self { Self::from_kilometers(value.get()) }
}
impl From<imperial::Mile> for LightDay {
    fn from(value: imperial::Mile) -> Self { Self::from_miles(value.get()) }
}
impl From<AstronomicalUnit> for LightDay {
    fn from(value: AstronomicalUnit) -> Self { Self::from_au(value.get()) }
}
impl From<LightSecond> for LightDay {
    fn from(value: LightSecond) -> Self { Self::new(value.get() / SECONDS_PER_DAY) }
}
impl From<LightMinute> for LightDay {
    fn from(value: LightMinute) -> Self { Self::new(value.get() / MINUTES_PER_DAY) }
}
impl From<LightHour> for LightDay {
    fn from(value: LightHour) -> Self { Self::new(value.get() / HOURS_PER_DAY) }
}
impl From<LightDay> for si::Kilometer {
    fn from(value: LightDay) -> Self { Self::new(value.get() * KILOMETERS_PER_LIGHT_SECOND * SECONDS_PER_DAY) }
}

/// Distance light travels in a Week (7 days)
#[derive(Debug, Default, Clone, Copy)]
#[quantity(units = "lwk", alt = "light-weeks")]
pub struct LightWeek(f64);

impl LightWeek {
    pub fn new(value: f64) -> Self { Self(value) }
    pub fn get(&self) -> f64 { self.0 }

    pub fn from_kilometers(km: f64) -> Self { Self(km / KILOMETERS_PER_LIGHT_SECOND / SECONDS_PER_DAY / DAYS_PER_WEEK) }
    pub fn from_miles(miles: f64) -> Self { Self::from_kilometers(miles * KILOMETERS_PER_MILE) }
    pub fn from_light_years(ly: f64) -> Self { Self::from_light_days(ly * DAYS_PER_JULIAN_YEAR) }
    pub fn from_light_days(ld: f64) -> Self { Self(ld / DAYS_PER_WEEK) }
}

// From trait
impl From<si::Kilometer> for LightWeek {
    fn from(value: si::Kilometer) -> Self { Self::from_kilometers(value.get()) }
}
impl From<imperial::Mile> for LightWeek {
    fn from(value: imperial::Mile) -> Self { Self::from_miles(value.get()) }
}
impl From<LightYear> for LightWeek {
    fn from(value: LightYear) -> Self { Self::from_light_years(value.get()) }
}
impl From<LightMonth> for LightWeek {
    fn from(value: LightMonth) -> Self { Self::from_light_days(value.get() * DAYS_PER_MONTH) }
}
impl From<LightDay> for LightWeek {
    fn from(value: LightDay) -> Self { Self::from_light_days(value.get()) }
}
impl From<LightWeek> for si::Kilometer {
    fn from(value: LightWeek) -> Self { Self::new(value.get() * DAYS_PER_WEEK * SECONDS_PER_DAY * KILOMETERS_PER_LIGHT_SECOND) }
}

/// Distance light travels in a month (standard month is 30 days)
#[derive(Debug, Default, Clone, Copy)]
#[quantity(units = "light-months", alt = "lightmonths")]
pub struct LightMonth(f64);

impl LightMonth {
    pub fn new(value: f64) -> Self { Self(value) }
    pub fn get(&self) -> f64 { self.0 }

    pub fn from_kilometers(km: f64) -> Self { Self(km / KILOMETERS_PER_LIGHT_SECOND / SECONDS_PER_DAY / DAYS_PER_MONTH) }
    pub fn from_miles(miles: f64) -> Self { Self::from_kilometers(miles * KILOMETERS_PER_MILE) }
    pub fn from_light_years(ly: f64) -> Self { Self::from_light_days(ly * DAYS_PER_JULIAN_YEAR) }
    pub fn from_light_days(ld: f64) -> Self { Self(ld / DAYS_PER_MONTH) }
}

// From trait
impl From<si::Kilometer> for LightMonth {
    fn from(value: si::Kilometer) -> Self { Self::from_kilometers(value.get()) }
}
impl From<imperial::Mile> for LightMonth {
    fn from(value: imperial::Mile) -> Self { Self::from_miles(value.get()) }
}
impl From<LightYear> for LightMonth {
    fn from(value: LightYear) -> Self { Self::from_light_years(value.get()) }
}
impl From<LightDay> for LightMonth {
    fn from(value: LightDay) -> Self { Self::from_light_days(value.get()) }
}
impl From<LightMonth> for si::Kilometer {
    fn from(value: LightMonth) -> Self { Self::new(value.get() * DAYS_PER_MONTH * SECONDS_PER_DAY * KILOMETERS_PER_LIGHT_SECOND) }
}

/// One parallax second (approximately 3.26 light-years)
#[derive(Debug, Default, Clone, Copy)]
#[quantity(units = "pc", alt = "parsecs")]
pub struct Parsec(f64);

impl Parsec {
    pub fn new(value: f64) -> Self { Self(value) }
    pub fn get(&self) -> f64 { self.0 }

    pub fn from_light_years(ly: f64) -> Self { Self::new(ly * PARSECS_PER_LIGHT_YEAR) }
}

// From trait
impl From<LightYear> for Parsec {
    fn from(value: LightYear) -> Self { Self::from_light_years(value.get()) }
}

/// Average distance between the sun and the earth
#[derive(Debug, Default, Clone, Copy)]
#[quantity(units = "AU", alt = "astronomical units")]
pub struct AstronomicalUnit(f64);

impl AstronomicalUnit {
    pub fn new(value: f64) -> Self { Self(value) }
    pub fn get(&self) -> f64 { self.0 }

    pub fn from_kilometers(km: f64) -> Self { Self(km / KILOMETERS_PER_ASTRONOMICAL_UNIT) }
    pub fn from_miles(miles: f64) -> Self { Self::from_kilometers(miles * KILOMETERS_PER_MILE) }
    pub fn from_meters(meters: f64) -> Self { Self::from_kilometers(meters / 1000.0) }
    pub fn from_light_seconds(ls: f64) -> Self { Self::from_meters(ls * METERS_PER_LIGHT_SECOND) }
}

// From trait
impl From<si::Kilometer> for AstronomicalUnit {
    fn from(value: si::Kilometer) -> Self { Self::from_kilometers(value.get()) }
}
impl From<imperial::Mile> for AstronomicalUnit {
    fn from(value: imperial::Mile) -> Self { Self::from_miles(value.get()) }
}
impl From<si::Meter> for AstronomicalUnit {
    fn from(value: si::Meter) -> Self { Self::from_meters(value.get()) }
}
impl From<LightSecond> for AstronomicalUnit {
    fn from(value: LightSecond) -> Self { Self::from_light_seconds(value.get()) }
}
impl From<AstronomicalUnit> for si::Kilometer {
    fn from(value: AstronomicalUnit) -> Self { Self::new(value.get() * KILOMETERS_PER_ASTRONOMICAL_UNIT) }
}

#[cfg(test)]
mod tests {
    use super::*;
    use spectral::prelude::*;

    mod light_year {
        use super::*;

        #[test]
        fn from_kilometers() {
            let expected = LightYear::new(1.0);
            let actual: LightYear = si::Kilometer::new(KILOMETERS_PER_LIGHT_YEAR).into();
            assert_that!(actual).is_equal_to(expected);
        }

        #[test]
        fn from_miles() {
            let expected = LightYear::new(1.0);
            let actual: LightYear = imperial::Mile::new(KILOMETERS_PER_LIGHT_YEAR / KILOMETERS_PER_MILE).into();
            assert_that!(actual.get()).is_close_to(expected.get(), 0.001);
        }

        #[test]
        fn from_aus() {
            let expected = LightYear::new(1.002);
            let actual: LightYear = AstronomicalUnit::new(MINUTES_PER_DAY * DAYS_PER_JULIAN_YEAR / 8.3).into();
            assert_that!(actual.get()).is_close_to(expected.get(), 0.001);
        }

        #[test]
        fn from_light_day() {
            let expected = LightYear::new(1.0);
            let actual: LightYear = LightDay::new(DAYS_PER_JULIAN_YEAR).into();
            assert_that!(actual.get()).is_close_to(expected.get(), 0.001);
        }

        #[test]
        fn display() {
            let l = LightYear::new(2.0);
            assert_that!(format!("{l:#}")).is_equal_to(String::from("2 light-years"));
            assert_that!(format!("{l}")).is_equal_to(String::from("2 ly"));
        }
    }

    mod light_second {
        use super::*;

        #[test]
        fn from_kilometers() {
            let expected = LightSecond::new(1.0);
            let actual: LightSecond = si::Kilometer::new(KILOMETERS_PER_LIGHT_SECOND).into();
            assert_that!(actual).is_equal_to(expected);
        }

        #[test]
        fn from_miles() {
            let expected = LightSecond::new(1.0);
            let actual: LightSecond = imperial::Mile::new(KILOMETERS_PER_LIGHT_SECOND / KILOMETERS_PER_MILE).into();
            assert_that!(actual.get()).is_close_to(expected.get(), 0.001);
        }

        #[test]
        fn from_aus() {
            let expected = LightSecond::new(499.003);
            let actual: LightSecond = AstronomicalUnit::new(1.0).into();
            assert_that!(actual.get()).is_close_to(expected.get(), 0.001);
        }

        #[test]
        fn from_light_minute() {
            let expected = LightSecond::new(60.0);
            let actual: LightSecond = LightMinute::new(1.0).into();
            assert_that!(actual).is_equal_to(expected);
        }

        #[test]
        fn from_light_hour() {
            let expected = LightSecond::new(SECONDS_PER_HOUR);
            let actual: LightSecond = LightHour::new(1.0).into();
            assert_that!(actual).is_equal_to(expected);
        }

        #[test]
        fn from_light_day() {
            let expected = LightSecond::new(SECONDS_PER_DAY);
            let actual: LightSecond = LightDay::new(1.0).into();
            assert_that!(actual).is_equal_to(expected);
        }

        #[test]
        fn display() {
            let l = LightSecond::new(2.0);
            assert_that!(format!("{l:#}")).is_equal_to(String::from("2 light-seconds"));
            assert_that!(format!("{l}")).is_equal_to(String::from("2 ls"));
        }
    }

    mod light_minute {
        use super::*;

        #[test]
        fn from_kilometers() {
            let expected = LightMinute::new(1.0);
            let actual: LightMinute = si::Kilometer::new(KILOMETERS_PER_LIGHT_SECOND * SECONDS_PER_MINUTE).into();
            assert_that!(actual.get()).is_close_to(expected.get(), 0.001);
        }

        #[test]
        fn from_miles() {
            let expected = LightMinute::new(1.0);
            let actual: LightMinute = imperial::Mile::new(KILOMETERS_PER_LIGHT_SECOND / KILOMETERS_PER_MILE * SECONDS_PER_MINUTE).into();
            assert_that!(actual.get()).is_close_to(expected.get(), 0.001);
        }

        #[test]
        fn from_aus() {
            let expected = LightMinute::new(8.317);
            let actual: LightMinute = AstronomicalUnit::new(1.0).into();
            assert_that!(actual.get()).is_close_to(expected.get(), 0.001);
        }

        #[test]
        fn from_light_second() {
            let expected = LightMinute::new(1.0);
            let actual: LightMinute = LightSecond::new(SECONDS_PER_MINUTE).into();
            assert_that!(actual).is_equal_to(expected);
        }

        #[test]
        fn from_light_hour() {
            let expected = LightMinute::new(60.0);
            let actual: LightMinute = LightHour::new(1.0).into();
            assert_that!(actual).is_equal_to(expected);
        }

        #[test]
        fn from_light_day() {
            let expected = LightMinute::new(MINUTES_PER_DAY);
            let actual: LightMinute = LightDay::new(1.0).into();
            assert_that!(actual).is_equal_to(expected);
        }

        #[test]
        fn display() {
            let l = LightMinute::new(2.0);
            assert_that!(format!("{l:#}")).is_equal_to(String::from("2 light-minutes"));
            assert_that!(format!("{l}")).is_equal_to(String::from("2 lmin"));
        }
    }

    mod light_hour {
        use super::*;

        #[test]
        fn from_kilometers() {
            let expected = LightHour::new(1.0);
            let actual: LightHour = si::Kilometer::new(KILOMETERS_PER_LIGHT_SECOND * SECONDS_PER_HOUR).into();
            assert_that!(actual).is_equal_to(expected);
        }

        #[test]
        fn from_miles() {
            let expected = LightHour::new(1.0);
            let actual: LightHour = imperial::Mile::new(KILOMETERS_PER_LIGHT_SECOND / KILOMETERS_PER_MILE * SECONDS_PER_HOUR).into();
            assert_that!(actual.get()).is_close_to(expected.get(), 0.001);
        }

        #[test]
        fn from_aus() {
            let expected = LightHour::new(0.831);
            let actual: LightHour = AstronomicalUnit::new(6.0).into();
            assert_that!(actual.get()).is_close_to(expected.get(), 0.001);
        }

        #[test]
        fn from_light_second() {
            let expected = LightHour::new(1.0);
            let actual: LightHour = LightSecond::new(SECONDS_PER_HOUR).into();
            assert_that!(actual).is_equal_to(expected);
        }

        #[test]
        fn from_light_minute() {
            let expected = LightHour::new(1.0);
            let actual: LightHour = LightMinute::new(MINUTES_PER_HOUR).into();
            assert_that!(actual).is_equal_to(expected);
        }

        #[test]
        fn from_light_day() {
            let expected = LightHour::new(24.0);
            let actual: LightHour = LightDay::new(1.0).into();
            assert_that!(actual).is_equal_to(expected);
        }

        #[test]
        fn display() {
            let l = LightHour::new(2.0);
            assert_that!(format!("{l:#}")).is_equal_to(String::from("2 light-hours"));
            assert_that!(format!("{l}")).is_equal_to(String::from("2 lhr"));
        }
    }

    mod light_day {
        use super::*;

        #[test]
        fn from_kilometers() {
            let expected = LightDay::new(1.0);
            let actual: LightDay = si::Kilometer::new(KILOMETERS_PER_LIGHT_SECOND * SECONDS_PER_DAY).into();
            assert_that!(actual).is_equal_to(expected);
        }

        #[test]
        fn from_miles() {
            let expected = LightDay::new(1.0);
            let actual: LightDay = imperial::Mile::new(KILOMETERS_PER_LIGHT_SECOND / KILOMETERS_PER_MILE * SECONDS_PER_DAY).into();
            assert_that!(actual.get()).is_close_to(expected.get(), 0.001);
        }

        #[test]
        fn from_aus() {
            let expected = LightDay::new(5.775);
            let actual: LightDay = AstronomicalUnit::new(1000.0).into();
            assert_that!(actual.get()).is_close_to(expected.get(), 0.001);
        }

        #[test]
        fn from_light_second() {
            let expected = LightDay::new(1.0);
            let actual: LightDay = LightSecond::new(SECONDS_PER_DAY).into();
            assert_that!(actual).is_equal_to(expected);
        }

        #[test]
        fn from_light_minute() {
            let expected = LightDay::new(1.0);
            let actual: LightDay = LightMinute::new(MINUTES_PER_DAY).into();
            assert_that!(actual).is_equal_to(expected);
        }

        #[test]
        fn from_light_hour() {
            let expected = LightDay::new(1.0);
            let actual: LightDay = LightHour::new(HOURS_PER_DAY).into();
            assert_that!(actual).is_equal_to(expected);
        }

        #[test]
        fn display() {
            let l = LightDay::new(2.0);
            assert_that!(format!("{l:#}")).is_equal_to(String::from("2 light-days"));
            assert_that!(format!("{l}")).is_equal_to(String::from("2 ld"));
        }
    }

    mod light_week {
        use super::*;

        const SECONDS_PER_WEEK: f64 = SECONDS_PER_DAY * DAYS_PER_WEEK;

        #[test]
        fn from_kilometers() {
            let expected = LightWeek::new(1.0);
            let actual: LightWeek = si::Kilometer::new(KILOMETERS_PER_LIGHT_SECOND * SECONDS_PER_WEEK).into();
            assert_that!(actual).is_equal_to(expected);
        }

        #[test]
        fn from_miles() {
            let expected = LightWeek::new(1.0);
            let actual: LightWeek = imperial::Mile::new(KILOMETERS_PER_LIGHT_SECOND / KILOMETERS_PER_MILE * SECONDS_PER_WEEK).into();
            assert_that!(actual.get()).is_close_to(expected.get(), 0.001);
        }

        #[test]
        fn from_light_day() {
            let expected = LightWeek::new(1.0);
            let actual: LightWeek = LightDay::new(DAYS_PER_WEEK).into();
            assert_that!(actual).is_equal_to(expected);
        }

        #[test]
        fn from_light_year() {
            let expected = LightWeek::new(DAYS_PER_JULIAN_YEAR / DAYS_PER_WEEK);
            let actual: LightWeek = LightYear::new(1.0).into();
            assert_that!(actual.get()).is_close_to(expected.get(), 0.001);
        }

        #[test]
        fn display() {
            let l = LightWeek::new(2.0);
            assert_that!(format!("{l:#}")).is_equal_to(String::from("2 light-weeks"));
            assert_that!(format!("{l}")).is_equal_to(String::from("2 lwk"));
        }
    }

    mod light_month {
        use super::*;

        const SECONDS_PER_MONTH: f64 = SECONDS_PER_DAY * DAYS_PER_MONTH;

        #[test]
        fn from_kilometers() {
            let expected = LightMonth::new(1.0);
            let actual: LightMonth = si::Kilometer::new(KILOMETERS_PER_LIGHT_SECOND * SECONDS_PER_MONTH).into();
            assert_that!(actual).is_equal_to(expected);
        }

        #[test]
        fn from_miles() {
            let expected = LightMonth::new(1.0);
            let actual: LightMonth = imperial::Mile::new(KILOMETERS_PER_LIGHT_SECOND / KILOMETERS_PER_MILE * SECONDS_PER_MONTH).into();
            assert_that!(actual.get()).is_close_to(expected.get(), 0.001);
        }

        #[test]
        fn from_light_day() {
            let expected = LightMonth::new(1.0);
            let actual: LightMonth = LightDay::new(DAYS_PER_MONTH).into();
            assert_that!(actual).is_equal_to(expected);
        }

        #[test]
        fn from_light_year() {
            let expected = LightMonth::new(DAYS_PER_JULIAN_YEAR / DAYS_PER_MONTH);
            let actual: LightMonth = LightYear::new(1.0).into();
            assert_that!(actual.get()).is_close_to(expected.get(), 0.001);
        }

        #[test]
        fn display() {
            let l = LightMonth::new(2.0);
            assert_that!(format!("{l:#}")).is_equal_to(String::from("2 lightmonths"));
            assert_that!(format!("{l}")).is_equal_to(String::from("2 light-months"));
        }
    }

    mod parsec {
        use super::*;

        #[test]
        fn from_light_years() {
            let expected = Parsec::new(1.0);
            let actual: Parsec = LightYear::new(1.0 / PARSECS_PER_LIGHT_YEAR).into();
            assert_that!(actual).is_equal_to(expected);
        }

        #[test]
        fn display() {
            let l = Parsec::new(2.0);
            assert_that!(format!("{l:#}")).is_equal_to(String::from("2 parsecs"));
            assert_that!(format!("{l}")).is_equal_to(String::from("2 pc"));
        }
    }

    mod au {
        use super::*;

        #[test]
        fn from_kilometers() {
            let m: AstronomicalUnit = si::Kilometer::new(2.0 * KILOMETERS_PER_ASTRONOMICAL_UNIT).into();
            assert_that!(m.get()).is_equal_to(2.0);
        }

        #[test]
        fn from_meters() {
            let m: AstronomicalUnit = si::Meter::new(1_500.0 * KILOMETERS_PER_ASTRONOMICAL_UNIT).into();
            assert_that!(m.get()).is_equal_to(1.5);
        }

        #[test]
        fn from_miles() {
            let m: AstronomicalUnit = imperial::Mile::new(93_000_000.0).into();
            assert_that!(m.get()).is_close_to(1.0, 0.001);
        }

        #[test]
        fn from_light_seconds() {
            let m: AstronomicalUnit = LightSecond::new(499.0).into();
            assert_that!(m.get()).is_close_to(1.0, 0.001);
        }

        #[test]
        fn display() {
            let l = AstronomicalUnit::new(2.0);
            assert_that!(format!("{l:#}")).is_equal_to(String::from("2 astronomical units"));
            assert_that!(format!("{l}")).is_equal_to(String::from("2 AU"));
        }
    }
}
