//! Length quantities for imperial units (standard in the US)
//!
//! * Mile
//! * Yard
//! * Foot
//! * Inch
//!
//! # Examples
//!
//! ```rust
//! use quantities::length::{imperial::Mile, si::Kilometer};
//!
//! let mut dist = Mile::new(1.0);
//! dist *= 2.0;
//!
//! let dist = Mile::new(55.0);
//! let km: Kilometer = dist.into();
//! ```

use crate::length::si;
use crate::length::convert::*;

use quantity_macro::quantity;

// Historical: Furlongs, Cubit

#[derive(Debug, Default, Clone, Copy)]
#[quantity(units = "mi", alt = "miles")]
pub struct Mile(f64);

impl Mile {
    pub fn new(value: f64) -> Self { Self(value) }
    pub fn get(&self) -> f64 { self.0 }

    pub fn from_yards(yards: f64) -> Self { Self(yards / YARDS_PER_MILE) }
    pub fn from_feet(feet: f64) -> Self { Self(feet / FEET_PER_MILE) }
    pub fn from_kilometers(klick: f64) -> Self { Self(klick / KILOMETERS_PER_MILE) }
    pub fn from_meters(meters: f64) -> Self { Self(meters / METERS_PER_MILE) }
}

// From trait
impl From<Yard> for Mile {
    fn from(value: Yard) -> Self { Self::from_yards(value.get()) }
}
impl From<Foot> for Mile {
    fn from(value: Foot) -> Self { Self::from_feet(value.get()) }
}
impl From<Inch> for Mile {
    fn from(value: Inch) -> Self { Self::from_feet(value.get() / INCHES_PER_FOOT) }
}
impl From<si::Kilometer> for Mile {
    fn from(value: si::Kilometer) -> Self { Self::from_kilometers(value.get()) }
}
impl From<si::Meter> for Mile {
    fn from(value: si::Meter) -> Self { Self::from_meters(value.get()) }
}

#[derive(Debug, Default, Clone, Copy)]
#[quantity(units = "yd", alt = "yards")]
pub struct Yard(f64);

impl Yard {
    pub fn new(value: f64) -> Self { Self(value) }
    pub fn get(&self) -> f64 { self.0 }

    pub fn from_miles(miles: f64) -> Self { Self(miles * YARDS_PER_MILE) }
    pub fn from_feet(feet: f64) -> Self { Self(feet / FEET_PER_YARD) }
    pub fn from_inches(inches: f64) -> Self { Self(inches / INCHES_PER_YARD) }
    pub fn from_kilometers(klicks: f64) -> Self { Self(klicks / KILOMETERS_PER_MILE * YARDS_PER_MILE) }
    pub fn from_meters(meters: f64) -> Self { Self(meters * METERS_PER_MILE / YARDS_PER_MILE) }
}

// From trait
impl From<Mile> for Yard {
    fn from(value: Mile) -> Self { Self::from_miles(value.get()) }
}
impl From<Foot> for Yard {
    fn from(value: Foot) -> Self { Self::from_feet(value.get()) }
}
impl From<Inch> for Yard {
    fn from(value: Inch) -> Self { Self::from_inches(value.get()) }
}
impl From<si::Kilometer> for Yard {
    fn from(value: si::Kilometer) -> Self { Self::from_kilometers(value.get()) }
}
impl From<si::Meter> for Yard {
    fn from(value: si::Meter) -> Self { Self::from_meters(value.get()) }
}

#[derive(Debug, Default, Clone, Copy)]
#[quantity(units = "ft", alt = "feet")]
pub struct Foot(f64);

impl Foot {
    pub fn new(value: f64) -> Self { Self(value) }
    pub fn get(&self) -> f64 { self.0 }

    pub fn from_miles(miles: f64) -> Self { Self(miles * FEET_PER_MILE) }
    pub fn from_yards(yards: f64) -> Self { Self(yards * FEET_PER_YARD) }
    pub fn from_inches(inches: f64) -> Self { Self(inches / INCHES_PER_FOOT) }
    pub fn from_meters(meters: f64) -> Self { Self::new(meters * FEET_PER_METER) }
    pub fn from_centimeters(cm: f64) -> Self { Self::from_inches(cm / CM_PER_INCH) }
}

// From trait
impl From<Mile> for Foot {
    fn from(value: Mile) -> Self { Self::from_miles(value.get()) }
}
impl From<Yard> for Foot {
    fn from(value: Yard) -> Self { Self::from_yards(value.get()) }
}
impl From<Inch> for Foot {
    fn from(value: Inch) -> Self { Self::from_inches(value.get()) }
}
impl From<si::Meter> for Foot {
    fn from(value: si::Meter) -> Self { Self::from_meters(value.get()) }
}
impl From<si::Centimeter> for Foot {
    fn from(value: si::Centimeter) -> Self { Self::from_centimeters(value.get()) }
}

#[derive(Debug, Default, Clone, Copy)]
#[quantity(units = "in", alt = "inches")]
pub struct Inch(f64);

impl Inch {
    pub fn new(value: f64) -> Self { Self(value) }
    pub fn get(&self) -> f64 { self.0 }

    pub fn from_feet(feet: f64) -> Self { Self(feet * INCHES_PER_FOOT) }
    pub fn from_yards(yards: f64) -> Self { Self(yards * INCHES_PER_YARD) }
    pub fn from_meters(meters: f64) -> Self { Self(meters * INCHES_PER_METER) }
    pub fn from_centimeters(cm: f64) -> Self { Self(cm / CM_PER_INCH) }
    pub fn from_millimeters(mm: f64) -> Self { Self(mm / MM_PER_INCH) }
}

// From trait
impl From<Yard> for Inch {
    fn from(value: Yard) -> Self { Self::from_yards(value.get()) }
}
impl From<Foot> for Inch {
    fn from(value: Foot) -> Self { Self::from_feet(value.get()) }
}
impl From<si::Kilometer> for Inch {
    fn from(value: si::Kilometer) -> Self { Self::from_meters(value.get() * 1000.0) }
}
impl From<si::Meter> for Inch {
    fn from(value: si::Meter) -> Self { Self::from_meters(value.get()) }
}
impl From<si::Centimeter> for Inch {
    fn from(value: si::Centimeter) -> Self { Self::from_centimeters(value.get()) }
}
impl From<si::Millimeter> for Inch {
    fn from(value: si::Millimeter) -> Self { Self::from_millimeters(value.get()) }
}

#[cfg(test)]
mod tests {
    use super::*;
    use spectral::prelude::*;

    mod mile {
        use super::*;

        #[test]
        fn new() {
            assert_that!(Mile::new(1.0).get()).named("new: 1.0").is_equal_to(1.0);
            assert_that!(Mile::new(1.5).get()).named("new: 1.5").is_equal_to(1.5);
        }

        #[test]
        fn from_yards() {
            assert_that!(Mile::from_yards(880.0).get()).is_equal_to(0.5);
            assert_that!(Mile::from_yards(1000.0).get()).is_close_to(0.568, 0.001);
        }

        #[test]
        fn from_feet() {
            assert_that!(Mile::from_feet(3960.0).get()).is_equal_to(0.75);
            assert_that!(Mile::from_feet(5280.0).get()).is_equal_to(1.0);
        }

        #[test]
        fn from_kilometers() {
            assert_that!(Mile::from_kilometers(8.0).get()).is_close_to(4.97, 0.001);
            assert_that!(Mile::from_kilometers(100.0).get()).is_close_to(62.137, 0.001);
        }

        #[test]
        fn from_meters() {
            assert_that!(Mile::from_meters(880.0).get()).is_close_to(0.547, 0.001);
            assert_that!(Mile::from_meters(1500.0).get()).is_close_to(0.932, 0.001);
        }

        #[test]
        fn add() {
            let m = Mile::new(1.0) + Mile::new(3.5);
            assert_that!(m.get()).named("1 + 3.5").is_equal_to(4.5);
        }

        #[test]
        fn add_feet() {
            let m = Mile::new(1.5) + Foot::new(5280.0);
            assert_that!(m.get()).named("1.5 + 5280ft").is_equal_to(2.5);
        }

        #[test]
        fn subtract() {
            let m = Mile::new(3.5) - Mile::new(2.25);
            assert_that!(m.get()).named("3.5 - 2.25").is_equal_to(1.25);
        }

        #[test]
        fn subtract_yards() {
            let m = Mile::new(3.5) - Yard::new(5280.0);
            assert_that!(m.get()).named("3.5 - 5280yd").is_equal_to(0.5);
        }

        #[test]
        fn add_assign() {
            let mut m = Mile::new(1.0);
            m += Mile::new(3.5);
            assert_that!(m.get()).named("1 + 3.5").is_equal_to(4.5);
        }

        #[test]
        fn add_assign_yards() {
            let mut m = Mile::new(1.0);
            m += Yard::new(5280.0);
            assert_that!(m.get()).named("1 + 5280yd").is_equal_to(4.0);
        }

        #[test]
        fn subtract_assign() {
            let mut m = Mile::new(3.5);
            m -= Mile::new(2.25);
            assert_that!(m.get()).named("3.5 - 2.25").is_equal_to(1.25);
        }

        #[test]
        fn subtract_assign_feet() {
            let mut m = Mile::new(3.5);
            m -= Foot::new(5280.0);
            assert_that!(m.get()).named("3.5 - 5280ft").is_equal_to(2.5);
        }

        #[test]
        fn multiply() {
            let m = Mile::new(2.0) * 5.0;
            assert_that!(m.get()).named("Mile(2.0) * 5").is_equal_to(10.0);
            let m = 3.0 * Mile::new(2.5);
            assert_that!(m.get()).named("3.0 * Mile(2.5)").is_equal_to(7.5);
        }

        #[test]
        fn multiply_assign() {
            let mut m = Mile::new(2.0);
            m *= 3.0;
            assert_that!(m.get()).named("Mile(2.0) *= 3").is_equal_to(6.0);
        }

        #[test]
        fn divide() {
            let m = Mile::new(5.0) / 2.0;
            assert_that!(m.get()).named("Mile(5.0) / 2").is_equal_to(2.5);
        }

        #[test]
        fn divide_assign() {
            let mut m = Mile::new(3.0);
            m /= 2.0;
            assert_that!(m.get()).named("Mile(3.0) /= 2").is_equal_to(1.5);
        }

        #[test]
        fn display() {
            let l = Mile::new(2.0);
            assert_that!(format!("{l:#}")).is_equal_to(String::from("2 miles"));
            assert_that!(format!("{l}")).is_equal_to(String::from("2 mi"));
        }
    }

    mod yard {
        use super::*;

        #[test]
        fn new() {
            assert_that!(Yard::new(1.0).get()).named("new: 1.0").is_equal_to(1.0);
            assert_that!(Yard::new(1.5).get()).named("new: 1.5").is_equal_to(1.5);
        }

        #[test]
        fn from_miles() {
            assert_that!(Yard::from_miles(0.5).get()).is_equal_to(880.0);
            assert_that!(Yard::from_miles(0.75).get()).is_equal_to(1320.0);
        }

        #[test]
        fn from_feet() {
            assert_that!(Yard::from_feet(39.0).get()).is_equal_to(13.0);
            assert_that!(Yard::from_feet(51.0).get()).is_equal_to(17.0);
        }

        #[test]
        fn from_kilometers() {
            assert_that!(Yard::from_kilometers(0.5).get()).is_close_to(546.808, 0.001);
            assert_that!(Yard::from_kilometers(1.0).get()).is_close_to(1093.616, 0.001);
        }

        #[test]
        fn from_meters() {
            assert_that!(Yard::from_meters(3.0).get()).is_close_to(2.743, 0.001);
            assert_that!(Yard::from_meters(10.0).get()).is_close_to(9.144, 0.001);
        }

        #[test]
        fn add() {
            let m = Yard::new(1.0) + Yard::new(3.5);
            assert_that!(m.get()).named("1 + 3.5").is_equal_to(4.5);
        }

        #[test]
        fn subtract() {
            let m = Yard::new(3.5) - Yard::new(2.25);
            assert_that!(m.get()).named("3.5 - 2.25").is_equal_to(1.25);
        }

        #[test]
        fn add_assign() {
            let mut m = Yard::new(1.0);
            m += Yard::new(3.5);
            assert_that!(m.get()).named("1 + 3.5").is_equal_to(4.5);
        }

        #[test]
        fn subtract_assign() {
            let mut m = Yard::new(3.5);
            m -= Yard::new(2.25);
            assert_that!(m.get()).named("3.5 - 2.25").is_equal_to(1.25);
        }

        #[test]
        fn multiply() {
            let m = Yard::new(2.0) * 5.0;
            assert_that!(m.get()).named("Yard(2.0) * 5").is_equal_to(10.0);
            let m = 3.0 * Yard::new(2.5);
            assert_that!(m.get()).named("3.0 * Yard(2.5)").is_equal_to(7.5);
        }

        #[test]
        fn multiply_assign() {
            let mut m = Yard::new(2.0);
            m *= 3.0;
            assert_that!(m.get()).named("Yard(2.0) *= 3").is_equal_to(6.0);
        }

        #[test]
        fn divide() {
            let m = Yard::new(5.0) / 2.0;
            assert_that!(m.get()).named("Yard(5.0) / 2").is_equal_to(2.5);
        }

        #[test]
        fn divide_assign() {
            let mut m = Yard::new(3.0);
            m /= 2.0;
            assert_that!(m.get()).named("Yard(3.0) /= 2").is_equal_to(1.5);
        }

        #[test]
        fn display() {
            let l = Yard::new(2.0);
            assert_that!(format!("{l:#}")).is_equal_to(String::from("2 yards"));
            assert_that!(format!("{l}")).is_equal_to(String::from("2 yd"));
        }
    }

    mod foot {
        use super::*;

        #[test]
        fn new() {
            assert_that!(Foot::new(1.0).get()).named("new: 1.0").is_equal_to(1.0);
            assert_that!(Foot::new(1.5).get()).named("new: 1.5").is_equal_to(1.5);
        }

        #[test]
        fn from_miles() {
            assert_that!(Foot::from_miles(0.5).get()).is_equal_to(5280.0 / 2.0);
            assert_that!(Foot::from_miles(0.75).get()).is_equal_to(3.0 * 5280.0 / 4.0);
        }

        #[test]
        fn from_yards() {
            assert_that!(Foot::from_yards(13.0).get()).is_equal_to(39.0);
            assert_that!(Foot::from_yards(17.0).get()).is_equal_to(51.0);
        }

        #[test]
        fn from_inches() {
            assert_that!(Foot::from_inches(36.0).get()).is_equal_to(3.0);
            assert_that!(Foot::from_inches(18.0).get()).is_equal_to(1.5);
        }

        #[test]
        fn from_meters() {
            assert_that!(Foot::from_meters(3.0).get()).is_close_to(9.843, 0.001);
            assert_that!(Foot::from_meters(10.0).get()).is_close_to(32.808, 0.001);
        }

        #[test]
        fn add() {
            let m = Foot::new(1.0) + Foot::new(3.5);
            assert_that!(m.get()).named("1 + 3.5").is_equal_to(4.5);
        }

        #[test]
        fn subtract() {
            let m = Foot::new(3.5) - Foot::new(2.25);
            assert_that!(m.get()).named("3.5 - 2.25").is_equal_to(1.25);
        }

        #[test]
        fn add_assign() {
            let mut m = Foot::new(1.0);
            m += Foot::new(3.5);
            assert_that!(m.get()).named("1 + 3.5").is_equal_to(4.5);
        }

        #[test]
        fn subtract_assign() {
            let mut m = Foot::new(3.5);
            m -= Foot::new(2.25);
            assert_that!(m.get()).named("3.5 - 2.25").is_equal_to(1.25);
        }

        #[test]
        fn multiply() {
            let m = Foot::new(2.0) * 5.0;
            assert_that!(m.get()).named("Foot(2.0) * 5").is_equal_to(10.0);
            let m = 3.0 * Foot::new(2.5);
            assert_that!(m.get()).named("3.0 * Foot(2.5)").is_equal_to(7.5);
        }

        #[test]
        fn multiply_assign() {
            let mut m = Foot::new(2.0);
            m *= 3.0;
            assert_that!(m.get()).named("Foot(2.0) *= 3").is_equal_to(6.0);
        }

        #[test]
        fn divide() {
            let m = Foot::new(5.0) / 2.0;
            assert_that!(m.get()).named("Foot(5.0) / 2").is_equal_to(2.5);
        }

        #[test]
        fn divide_assign() {
            let mut m = Foot::new(3.0);
            m /= 2.0;
            assert_that!(m.get()).named("Foot(3.0) /= 2").is_equal_to(1.5);
        }

        #[test]
        fn display() {
            let l = Foot::new(2.0);
            assert_that!(format!("{l:#}")).is_equal_to(String::from("2 feet"));
            assert_that!(format!("{l}")).is_equal_to(String::from("2 ft"));
        }
    }

    mod inch {
        use super::*;

        #[test]
        fn new() {
            assert_that!(Inch::new(1.0).get()).named("new: 1.0").is_equal_to(1.0);
            assert_that!(Inch::new(1.5).get()).named("new: 1.5").is_equal_to(1.5);
        }

        #[test]
        fn from_yards() {
            assert_that!(Inch::from_yards(1.0).get()).is_equal_to(36.0);
            assert_that!(Inch::from_yards(2.5).get()).is_equal_to(90.0);
        }

        #[test]
        fn from_meters() {
            assert_that!(Inch::from_meters(1.0).get()).is_close_to(39.37, 0.001);
            assert_that!(Inch::from_meters(2.25).get()).is_close_to(88.583, 0.001);
        }

        #[test]
        fn from_centimeters() {
            assert_that!(Inch::from_centimeters(2.54).get()).is_equal_to(1.0);
            assert_that!(Inch::from_centimeters(10.0).get()).is_close_to(3.937, 0.001);
        }

        #[test]
        fn from_millimeters() {
            assert_that!(Inch::from_millimeters(25.4).get()).is_equal_to(1.0);
            assert_that!(Inch::from_millimeters(100.0).get()).is_close_to(3.937, 0.001);
        }

        #[test]
        fn add() {
            let m = Inch::new(1.0) + Inch::new(3.5);
            assert_that!(m.get()).named("1 + 3.5").is_equal_to(4.5);
        }

        #[test]
        fn subtract() {
            let m = Inch::new(3.5) - Inch::new(2.25);
            assert_that!(m.get()).named("3.5 - 2.25").is_equal_to(1.25);
        }

        #[test]
        fn add_assign() {
            let mut m = Inch::new(1.0);
            m += Inch::new(3.5);
            assert_that!(m.get()).named("1 + 3.5").is_equal_to(4.5);
        }

        #[test]
        fn subtract_assign() {
            let mut m = Inch::new(3.5);
            m -= Inch::new(2.25);
            assert_that!(m.get()).named("3.5 - 2.25").is_equal_to(1.25);
        }

        #[test]
        fn multiply() {
            let m = Inch::new(2.0) * 5.0;
            assert_that!(m.get()).named("Inch(2.0) * 5").is_equal_to(10.0);
            let m = 3.0 * Inch::new(2.5);
            assert_that!(m.get()).named("3.0 * Inch(2.5)").is_equal_to(7.5);
        }

        #[test]
        fn multiply_assign() {
            let mut m = Inch::new(2.0);
            m *= 3.0;
            assert_that!(m.get()).named("Inch(2.0) *= 3").is_equal_to(6.0);
        }

        #[test]
        fn divide() {
            let m = Inch::new(5.0) / 2.0;
            assert_that!(m.get()).named("Inch(5.0) / 2").is_equal_to(2.5);
        }

        #[test]
        fn divide_assign() {
            let mut m = Inch::new(3.0);
            m /= 2.0;
            assert_that!(m.get()).named("Inch(3.0) /= 2").is_equal_to(1.5);
        }

        #[test]
        fn display() {
            let l = Inch::new(2.0);
            assert_that!(format!("{l:#}")).is_equal_to(String::from("2 inches"));
            assert_that!(format!("{l}")).is_equal_to(String::from("2 in"));
        }
    }
}
