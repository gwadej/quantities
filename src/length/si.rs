//! Length quantities for SI units
//!
//! * Kilometer
//! * Meter
//! * Centimeter
//! * Millimeter
//!
//! # Examples
//!
//! ```rust
//! use quantities::length::si::{Meter, Kilometer};
//! let dist = Kilometer::new(2.0);
//! let d = Meter::new(500.0);
//! println!("{} - {} = {}", &dist, &d, dist - d);
//! ```

use crate::length::imperial;
use crate::length::convert::*;

use quantity_macro::quantity;

#[derive(Debug, Default, Clone, Copy)]
#[quantity(units = "km", alt = "kilometers")]
pub struct Kilometer(f64);

impl Kilometer {
    pub fn new(value: f64) -> Self { Self(value) }
    pub fn get(&self) -> f64 { self.0 }

    pub fn from_miles(miles: f64) -> Self { Self(miles * KILOMETERS_PER_MILE) }
    pub fn from_meters(m: f64) -> Self { Self(m / 1000.0) }
    pub fn from_centimeters(cm: f64) -> Self { Self(cm / 100_000.0) }
    pub fn from_millimeters(mm: f64) -> Self { Self(mm / 1_000_000.0) }
}

// From trait
impl From<Meter> for Kilometer {
    fn from(value: Meter) -> Self { Self::from_meters(value.get()) }
}
impl From<Centimeter> for Kilometer {
    fn from(value: Centimeter) -> Self { Self::from_centimeters(value.get()) }
}
impl From<Millimeter> for Kilometer {
    fn from(value: Millimeter) -> Self { Self::from_millimeters(value.get()) }
}
impl From<imperial::Mile> for Kilometer {
    fn from(value: imperial::Mile) -> Self { Self::from_miles(value.get()) }
}
impl From<imperial::Yard> for Kilometer {
    fn from(value: imperial::Yard) -> Self { Self::from_meters(value.get() / YARDS_PER_METER) }
}
impl From<imperial::Foot> for Kilometer {
    fn from(value: imperial::Foot) -> Self { Self::from_meters(value.get() / FEET_PER_METER) }
}

#[derive(Debug, Default, Clone, Copy)]
#[quantity(units = "m", alt = "meters")]
pub struct Meter(f64);

impl Meter {
    pub fn new(value: f64) -> Self { Self(value) }
    pub fn get(&self) -> f64 { self.0 }

    pub fn from_kilometers(km: f64) -> Self { Self(km * 1000.0) }
    pub fn from_centimeters(cm: f64) -> Self { Self(cm / 100.0) }
    pub fn from_millimeters(mm: f64) -> Self { Self(mm / 1000.0) }
    pub fn from_yards(yards: f64) -> Self { Self(yards/ YARDS_PER_METER) }
    pub fn from_feet(feet: f64) -> Self { Self(feet / FEET_PER_METER) }
}

// From trait
impl From<Kilometer> for Meter {
    fn from(value: Kilometer) -> Self { Self::from_kilometers(value.get()) }
}
impl From<Centimeter> for Meter {
    fn from(value: Centimeter) -> Self { Self::from_centimeters(value.get()) }
}
impl From<Millimeter> for Meter {
    fn from(value: Millimeter) -> Self { Self::from_millimeters(value.get()) }
}
impl From<imperial::Mile> for Meter {
    fn from(value: imperial::Mile) -> Self { Self::from_kilometers(value.get() * KILOMETERS_PER_MILE) }
}
impl From<imperial::Yard> for Meter {
    fn from(value: imperial::Yard) -> Self { Self::from_yards(value.get()) }
}
impl From<imperial::Foot> for Meter {
    fn from(value: imperial::Foot) -> Self { Self::from_feet(value.get()) }
}

#[derive(Debug, Default, Clone, Copy)]
#[quantity(units = "cm", alt = "centimeters")]
pub struct Centimeter(f64);

impl Centimeter {
    pub fn new(value: f64) -> Self { Self(value) }
    pub fn get(&self) -> f64 { self.0 }

    pub fn from_meters(m: f64) -> Self { Self(m * 100.0) }
    pub fn from_millimeters(mm: f64) -> Self { Self(mm / 10.0) }
    pub fn from_yards(yards: f64) -> Self { Self::from_inches(yards * INCHES_PER_YARD) }
    pub fn from_feet(feet: f64) -> Self { Self::from_inches(feet * INCHES_PER_FOOT) }
    pub fn from_inches(inches: f64) -> Self { Self(inches * CM_PER_INCH) }
}

// From trait
impl From<Meter> for Centimeter {
    fn from(value: Meter) -> Self { Self::from_meters(value.get()) }
}
impl From<Millimeter> for Centimeter {
    fn from(value: Millimeter) -> Self { Self::from_millimeters(value.get()) }
}
impl From<imperial::Yard> for Centimeter {
    fn from(value: imperial::Yard) -> Self { Self::from_yards(value.get()) }
}
impl From<imperial::Foot> for Centimeter {
    fn from(value: imperial::Foot) -> Self { Self::from_feet(value.get()) }
}
impl From<imperial::Inch> for Centimeter {
    fn from(value: imperial::Inch) -> Self { Self::from_inches(value.get()) }
}

#[derive(Debug, Default, Clone, Copy)]
#[quantity(units = "mm", alt = "millimeters")]
pub struct Millimeter(f64);

impl Millimeter {
    pub fn new(value: f64) -> Self { Self(value) }
    pub fn get(&self) -> f64 { self.0 }

    pub fn from_meters(m: f64) -> Self { Self(m * 1000.0) }
    pub fn from_centimeters(cm: f64) -> Self { Self(cm * 10.0) }
    pub fn from_feet(feet: f64) -> Self { Self::from_inches(feet * INCHES_PER_FOOT) }
    pub fn from_inches(inches: f64) -> Self { Self(inches * MM_PER_INCH) }
}

// From trait
impl From<Meter> for Millimeter {
    fn from(value: Meter) -> Self { Self::from_meters(value.get()) }
}
impl From<Centimeter> for Millimeter {
    fn from(value: Centimeter) -> Self { Self::from_centimeters(value.get()) }
}
impl From<imperial::Foot> for Millimeter {
    fn from(value: imperial::Foot) -> Self { Self::from_feet(value.get()) }
}
impl From<imperial::Inch> for Millimeter {
    fn from(value: imperial::Inch) -> Self { Self::from_inches(value.get()) }
}

#[cfg(test)]
mod tests {
    use super::*;
    use spectral::prelude::*;

    mod kilometer {
        use super::*;

        #[test]
        fn new() {
            assert_that!(Kilometer::new(1.0).get()).named("new: 1.0").is_equal_to(1.0);
            assert_that!(Kilometer::new(1.5).get()).named("new: 1.5").is_equal_to(1.5);
        }

        #[test]
        fn from_miles() {
            assert_that!(Kilometer::from_miles(50.0).get()).is_close_to(80.467, 0.001);
            assert_that!(Kilometer::from_miles(6.7).get()).is_close_to(10.783, 0.001);
        }

        #[test]
        fn from_meters() {
            assert_that!(Kilometer::from_meters(3960.0).get()).is_close_to(3.96, 0.001);
            assert_that!(Kilometer::from_meters(528.0).get()).is_close_to(0.528, 0.001);
        }

        #[test]
        fn from_centimeters() {
            assert_that!(Kilometer::from_centimeters(800_000.0).get()).is_close_to(8.0, 0.001);
            assert_that!(Kilometer::from_centimeters(1_500_000.0).get()).is_equal_to(15.0);
        }

        #[test]
        fn from_millimeters() {
            assert_that!(Kilometer::from_millimeters(880_000.0).get()).is_close_to(0.880, 0.001);
            assert_that!(Kilometer::from_millimeters(1_500_000.0).get()).is_equal_to(1.5);
        }

        #[test]
        fn display() {
            let l = Kilometer::new(2.0);
            assert_that!(format!("{l:#}")).is_equal_to(String::from("2 kilometers"));
            assert_that!(format!("{l}")).is_equal_to(String::from("2 km"));
        }
    }

    mod meter {
        use super::*;

        #[test]
        fn new() {
            assert_that!(Meter::new(1.0).get()).named("new: 1.0").is_equal_to(1.0);
            assert_that!(Meter::new(1.5).get()).named("new: 1.5").is_equal_to(1.5);
        }

        #[test]
        fn from_kilometers() {
            assert_that!(Meter::from_kilometers(3.96).get()).is_close_to(3960.0, 0.001);
            assert_that!(Meter::from_kilometers(0.528).get()).is_close_to(528.0, 0.001);
        }

        #[test]
        fn from_centimeters() {
            assert_that!(Meter::from_centimeters(800.0).get()).is_close_to(8.0, 0.001);
            assert_that!(Meter::from_centimeters(1_500.0).get()).is_equal_to(15.0);
        }

        #[test]
        fn from_millimeters() {
            assert_that!(Meter::from_millimeters(880_000.0).get()).is_close_to(880.0, 0.001);
            assert_that!(Meter::from_millimeters(1_500.0).get()).is_equal_to(1.5);
        }

        #[test]
        fn from_yards() {
            assert_that!(Meter::from_yards(5.4).get()).is_close_to(4.938, 0.001);
            assert_that!(Meter::from_yards(12.0).get()).is_close_to(10.973, 0.001);
        }

        #[test]
        fn from_feet() {
            assert_that!(Meter::from_feet(3.3).get()).is_close_to(1.006, 0.001);
            assert_that!(Meter::from_feet(2.5).get()).is_close_to(0.762, 0.001);
        }

        #[test]
        fn display() {
            let l = Meter::new(2.0);
            assert_that!(format!("{l:#}")).is_equal_to(String::from("2 meters"));
            assert_that!(format!("{l}")).is_equal_to(String::from("2 m"));
        }
    }

    mod centimeter {
        use super::*;

        #[test]
        fn new() {
            assert_that!(Centimeter::new(1.0).get()).named("new: 1.0").is_equal_to(1.0);
            assert_that!(Centimeter::new(1.5).get()).named("new: 1.5").is_equal_to(1.5);
        }

        #[test]
        fn from_meters() {
            assert_that!(Centimeter::from_meters(8.0).get()).is_equal_to(800.0);
            assert_that!(Centimeter::from_meters(1.5).get()).is_equal_to(150.0);
        }

        #[test]
        fn from_millimeters() {
            assert_that!(Centimeter::from_millimeters(85.0).get()).is_close_to(8.5, 0.001);
            assert_that!(Centimeter::from_millimeters(100.0).get()).is_equal_to(10.0);
        }

        #[test]
        fn from_yards() {
            assert_that!(Centimeter::from_yards(1.0).get()).is_close_to(91.44, 0.001);
            assert_that!(Centimeter::from_yards(1.5).get()).is_close_to(137.16, 0.001);
        }

        #[test]
        fn from_feet() {
            assert_that!(Centimeter::from_feet(3.0).get()).is_close_to(91.44, 0.001);
            assert_that!(Centimeter::from_feet(2.5).get()).is_close_to(76.2, 0.001);
        }

        #[test]
        fn from_inches() {
            assert_that!(Centimeter::from_inches(4.0).get()).is_close_to(10.16, 0.001);
            assert_that!(Centimeter::from_inches(1.5).get()).is_close_to(3.81, 0.001);
        }

        #[test]
        fn display() {
            let l = Centimeter::new(2.0);
            assert_that!(format!("{l:#}")).is_equal_to(String::from("2 centimeters"));
            assert_that!(format!("{l}")).is_equal_to(String::from("2 cm"));
        }
    }

    mod millimeter {
        use super::*;

        #[test]
        fn new() {
            assert_that!(Millimeter::new(1.0).get()).named("new: 1.0").is_equal_to(1.0);
            assert_that!(Millimeter::new(1.5).get()).named("new: 1.5").is_equal_to(1.5);
        }

        #[test]
        fn from_meters() {
            assert_that!(Millimeter::from_meters(8.0).get()).is_equal_to(8000.0);
            assert_that!(Millimeter::from_meters(1.5).get()).is_equal_to(1500.0);
        }

        #[test]
        fn from_centimeters() {
            assert_that!(Millimeter::from_centimeters(8.50).get()).is_close_to(85.0, 0.001);
            assert_that!(Millimeter::from_centimeters(10.00).get()).is_equal_to(100.0);
        }

        #[test]
        fn from_feet() {
            assert_that!(Millimeter::from_feet(3.0).get()).is_close_to(914.4, 0.001);
            assert_that!(Millimeter::from_feet(2.5).get()).is_close_to(762.0, 0.001);
        }

        #[test]
        fn from_inches() {
            assert_that!(Millimeter::from_inches(4.0).get()).is_close_to(101.6, 0.001);
            assert_that!(Millimeter::from_inches(1.5).get()).is_close_to(38.1, 0.001);
        }

        #[test]
        fn display() {
            let l = Millimeter::new(2.0);
            assert_that!(format!("{l:#}")).is_equal_to(String::from("2 millimeters"));
            assert_that!(format!("{l}")).is_equal_to(String::from("2 mm"));
        }
    }
}
