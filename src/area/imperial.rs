//! Area quantities for imperial units (standard in the US)
//!
//! * SquareMile
//! * SquareYard
//! * SquareFoot
//! * SquareInch
//!
//! # Examples
//!
//! ```rust
//! use quantities::length::{imperial::Mile, si::Kilometer};
//!
//! let mut dist = Mile::new(1.0);
//! dist *= 2.0;
//!
//! let dist = Mile::new(55.0);
//! let km: Kilometer = dist.into();
//! ```

use crate::area::convert::*;

use crate::length::si;
//use crate::length::convert::*;

use quantity_macro::quantity;

// Historical: Furlongs, Cubit

#[derive(Debug, Default, Clone, Copy)]
#[quantity(units = "sqmi", alt = "square-miles")]
pub struct SquareMile(f64);

impl SquareMile {
    pub fn new(value: f64) -> Self { Self(value) }
    pub fn get(&self) -> f64 { self.0 }

    pub fn from_square_yards(sqyards: f64) -> Self { Self(sqyards / SQYD_PER_SQMILE) }
    pub fn from_square_feet(sqfeet: f64) -> Self { Self(sqfeet / SQFT_PER_SQMILE) }
    pub fn from_square_kilometers(sqkm: f64) -> Self { Self(sqkm / SQKM_PER_SQMILE) }
    pub fn from_square_meters(sqmeters: f64) -> Self { Self(sqmeters / SQM_PER_SQMILE) }
}

// From trait
impl From<SquareYard> for SquareMile {
    fn from(value: SquareYard) -> Self { Self::from_square_yards(value.get()) }
}
impl From<SquareFoot> for SquareMile {
    fn from(value: SquareFoot) -> Self { Self::from_square_feet(value.get()) }
}
impl From<SquareInch> for SquareMile {
    fn from(value: SquareInch) -> Self { Self::from_square_feet(value.get() / SQIN_PER_SQFT) }
}
// impl From<si::Kilometer> for SquareMile {
//     fn from(value: si::Kilometer) -> Self { Self::from_kilometers(value.get()) }
// }
// impl From<si::Meter> for SquareMile {
//     fn from(value: si::Meter) -> Self { Self::from_meters(value.get()) }
// }

#[derive(Debug, Default, Clone, Copy)]
#[quantity(units = "syd", alt = "square-yards")]
pub struct SquareYard(f64);

impl SquareYard {
    pub fn new(value: f64) -> Self { Self(value) }
    pub fn get(&self) -> f64 { self.0 }

    pub fn from_square_miles(sqmiles: f64) -> Self { Self(sqmiles * SQYD_PER_SQMILE) }
    pub fn from_square_feet(sqfeet: f64) -> Self { Self(sqfeet / SQFT_PER_SQYD) }
    pub fn from_square_inches(sqinches: f64) -> Self { Self(sqinches / SQIN_PER_SQYD) }
    pub fn from_square_kilometers(sqkm: f64) -> Self { Self(sqkm / SQKM_PER_SQMILE * SQYD_PER_SQMILE }
    pub fn from_square_meters(sqmeters: f64) -> Self { Self(sqmeters * SQM_PER_SQMILE / SQYD_PER_SQMILE) }
}

// From trait
impl From<SquareMile> for SquareYard {
    fn from(value: SquareMile) -> Self { Self::from_square_miles(value.get()) }
}
impl From<SquareFoot> for SquareYard {
    fn from(value: SquareFoot) -> Self { Self::from_square_feet(value.get()) }
}
impl From<SquareInch> for SquareYard {
    fn from(value: SquareInch) -> Self { Self::from_square_inches(value.get()) }
}
// impl From<si::Kilometer> for SquareYard {
//     fn from(value: si::Kilometer) -> Self { Self::from_kilometers(value.get()) }
// }
// impl From<si::Meter> for SquareYard {
//     fn from(value: si::Meter) -> Self { Self::from_meters(value.get()) }
// }

#[derive(Debug, Default, Clone, Copy)]
#[quantity(units = "ft", alt = "feet")]
pub struct SquareFoot(f64);

impl SquareFoot {
    pub fn new(value: f64) -> Self { Self(value) }
    pub fn get(&self) -> f64 { self.0 }

    pub fn from_square_miles(sqmiles: f64) -> Self { Self(sqmiles * SQFT_PER_SQMILE) }
    pub fn from_square_yards(sqyards: f64) -> Self { Self(sqyards * SQFT_PER_SQYD) }
    pub fn from_square_inches(sqinches: f64) -> Self { Self(sqinches / SQIN_PER_SQFT) }
    pub fn from_square_meters(sqmeters: f64) -> Self { Self::new(sqmeters * SQFT_PER_SQM) }
    pub fn from_square_centimeters(sqcm: f64) -> Self { Self::from_square_inches(sqcm / SQCM_PER_SQIN) }
}

// From trait
impl From<SquareMile> for SquareFoot {
    fn from(value: SquareMile) -> Self { Self::from_square_miles(value.get()) }
}
impl From<SquareYard> for SquareFoot {
    fn from(value: SquareYard) -> Self { Self::from_square_yards(value.get()) }
}
impl From<SquareInch> for SquareFoot {
    fn from(value: SquareInch) -> Self { Self::from_square_inches(value.get()) }
}
// impl From<si::Meter> for SquareFoot {
//     fn from(value: si::Meter) -> Self { Self::from_meters(value.get()) }
// }
// impl From<si::Centimeter> for SquareFoot {
//     fn from(value: si::Centimeter) -> Self { Self::from_centimeters(value.get()) }
// }

#[derive(Debug, Default, Clone, Copy)]
#[quantity(units = "in", alt = "inches")]
pub struct SquareInch(f64);

impl SquareInch {
    pub fn new(value: f64) -> Self { Self(value) }
    pub fn get(&self) -> f64 { self.0 }

    pub fn from_square_feet(sqfeet: f64) -> Self { Self(sqfeet * SQIN_PER_SQFT) }
    pub fn from_square_yards(sqyards: f64) -> Self { Self(sqyards * SQIN_PER_SQYD) }
    pub fn from_square_meters(sqmeters: f64) -> Self { Self(sqmeters * SQIN_PER_SQM) }
    pub fn from_square_centimeters(sqcm: f64) -> Self { Self(sqcm / SQCM_PER_SQIN) }
    pub fn from_square_millimeters(sqmm: f64) -> Self { Self(sqmm / SQMM_PER_SQIN) }
}

// From trait
impl From<SquareYard> for SquareInch {
    fn from(value: SquareYard) -> Self { Self::from_square_yards(value.get()) }
}
impl From<SquareFoot> for SquareInch {
    fn from(value: SquareFoot) -> Self { Self::from_square_feet(value.get()) }
}
// impl From<si::Meter> for SquareInch {
//     fn from(value: si::Meter) -> Self { Self::from_meters(value.get()) }
// }
// impl From<si::Centimeter> for SquareInch {
//     fn from(value: si::Centimeter) -> Self { Self::from_centimeters(value.get()) }
// }
// impl From<si::Millimeter> for SquareInch {
//     fn from(value: si::Millimeter) -> Self { Self::from_millimeters(value.get()) }
// }
