//! Definitions for length NewTypes

use crate::length::convert::*;

mod convert {
    pub(crate) const SQYD_PER_SQMILE: f64 = YARDS_PER_MILE * YARDS_PER_MILE;
    pub(crate) const SQFT_PER_SQMILE: f64 = FEET_PER_MILE * FEET_PER_MILE;
    pub(crate) const SQKM_PER_SQMILE: f64 = KILOMETERS_PER_MILE * KILOMETERS_PER_MILE;
    pub(crate) const SQM_PER_SQMILE: f64 = METERS_PER_MILE * METERS_PER_MILE;

    pub(crate) const SQFT_PER_SQYD: f64 = FEET_PER_YARD * FEET_PER_YARD;
    pub(crate) const SQFT_PER_SQM: f64 = FEET_PER_METER * FEET_PER_METER;

    pub(crate) const SQIN_PER_SQYD: f64 = INCHES_PER_YARD * INCHES_PER_YARD;
    pub(crate) const SQIN_PER_SQFT: f64 = INCHES_PER_FOOT * INCHES_PER_FOOT;
    pub(crate) const SQIN_PER_SQM: f64 = INCHES_PER_METER * INCHES_PER_METER;

    pub(crate) const SQCM_PER_SQIN: f64 = CM_PER_INCH * CM_PER_INCH;
    pub(crate) const SQMM_PER_SQIN: f64 = MM_PER_INCH * MM_PER_INCH;
}

pub mod imperial;

