//! Definitions for length types

// Conversion factors, so they aren't spread around
mod convert {
    pub(crate) const FEET_PER_MILE: f64 = 5280.0;
    pub(crate) const YARDS_PER_MILE: f64 = FEET_PER_MILE / FEET_PER_YARD;
    pub(crate) const METERS_PER_MILE: f64 = 1609.34;
    pub(crate) const KILOMETERS_PER_MILE: f64 = 1.60934;

    pub(crate) const FEET_PER_YARD: f64 = 3.0;
    pub(crate) const INCHES_PER_FOOT: f64 = 12.0;
    pub(crate) const INCHES_PER_YARD: f64 = 36.0;

    pub(crate) const INCHES_PER_METER: f64 = 39.3701;
    pub(crate) const YARDS_PER_METER: f64 = INCHES_PER_METER / INCHES_PER_YARD;
    pub(crate) const FEET_PER_METER: f64 = INCHES_PER_METER / INCHES_PER_FOOT;
    pub(crate) const CM_PER_INCH: f64 = 2.54;
    pub(crate) const MM_PER_INCH: f64 = 25.4;
}

pub mod imperial;
pub mod si;
pub mod astronomical;
