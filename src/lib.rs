//! Library for different kinds of quantities
//!
//! * area
//! * length

pub mod area;
pub mod length;

// Area: square-inch, square-foot, square-yard, square-mile, square-meter, square-kilometer
// Volume: ounce, pint, quart, cup, gallon, liter, milliliter
// Time: second, minute, hour, day, year
// Velocity: m/s, feet/s, mile/hr, km/hr, mm/s
// Acceleration: m/s^2, ft/s^2
