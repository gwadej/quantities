use quantities::length::imperial::{Mile, Foot};
use quantities::length::si::{Kilometer, Meter};

fn main() {
    let dist: Mile = Mile::new(10.0);

    println!("{}", dist * 2.0);
    println!("{:#}", 3.0 * dist);

    println!("{:.3}", Mile::from_meters(8000.0));
    println!("{:#.3}", Mile::from_kilometers(88.0));

    let a = Mile::new(1.0);
    let b = Foot::new(5280.0);
    println!("{:#} + {:#} = {}", a, b, a + b);
    let c = Kilometer::new(8.0);
    println!("{:#} + {:#} = {:.3}", a, c, a + c);
    let d = Meter::new(80_000.0);
    println!("{:#} + {:#} = {:.3}", a, d, a + d);
}
