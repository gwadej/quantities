use quantities::length::{astronomical, si};

fn main()
{
    let au = astronomical::AstronomicalUnit::new(1.0);
    let ls = astronomical::LightSecond::new(1.0);
    let lmin = astronomical::LightMinute::new(1.0);
    let lh = astronomical::LightHour::new(1.0);
    let ly = astronomical::LightYear::new(1.0);

    println!("{} = {:#}", au, si::Kilometer::from(au));
    println!("{} = {:#}", ls, si::Kilometer::from(ls));
    println!("{} = {:#}", lmin, si::Kilometer::from(lmin));
    println!("{} = {:#}", lh, si::Kilometer::from(lh));
    println!("{} = {:#}", ly, si::Kilometer::from(ly));
}
