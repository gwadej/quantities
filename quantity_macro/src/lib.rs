//! The [`macro@quantity`] attribute macro which adds some standard behaviors for the NewTypes in
//! this crate.
extern crate proc_macro;
extern crate proc_macro2;
extern crate syn;
extern crate quote;

use proc_macro::TokenStream;
use syn::{parse_macro_input, DeriveInput, Ident};
use syn::{Data, DataStruct, Field, Fields, FieldsUnnamed, Type, Visibility};
use quote::quote;

mod quantity_traits;
use quantity_traits::QuantityAttrs;

/// The [`macro@quantity`] attribute macro provides some standard behaviors for quantity types.
///
/// The quantity types must be tuple structs with a single member having an numeric type.
/// In the current implementation that is always `f64`. This restriction may be lifted
/// in the future.
///
/// The [`macro@quantity`] attribute macro provides implementations for traits from [`std::cmp`]
///
/// * `impl<Rhs> PartialOrd<Rhs> for Q where Rhs: Into<Q> + Copy`
/// * `impl<Rhs> PartialEq<Rhs> for Q where Rhs: Into<Q> + Copy`
///
/// The macro also provides implementations for the following traits from [`std::ops`]
/// Assume Q is the quantity type a V is the type of the tuple member. For example,
/// `Mile(f64)` would mean `Q` is `Mile` and `V` is `f64`.
///
/// * `impl<T> Add<T> for Q where T: Into<Q>`
/// * `impl<T> Sub<T> for Q where T: Into<Q>`
/// * `impl<T> AddAssign<T> for Q where T: Into<Q>`
/// * `impl<T> SubAssign<T> for Q where T: Into<Q>`
/// * `impl<V> Mul<V> for Q`
/// * `impl<V> Mul<Q> for V`
/// * `impl<V> MulAssign<V> for Q`
/// * `impl<V> Div<V> for Q`
/// * `impl<V> DivAssign<V> for Q`
///
/// Finally, the macro provides a [`std::fmt::Display`] trait implementation if the macro
/// specifies units (see below).
///
/// * `impl Display for Q`
///
/// This implementation applies the format modifiers to the numeric value of the quantity,
/// and adds units to the display. If you add the '#' alternate specifier, the alternate
/// units will be used.

/// The quantity attribute macro specifies the text used as units for the [`std::fmt::Display`] trait.
/// The attribute supports two arguments: `units` and `alt`. If neither are supplied, the
/// macro does not implement the [`std::fmt::Display`] trait.
///
/// # Example
///
/// ```rust
/// #[quantity(alt = "miles", units = "mi")]
/// pub struct Mile(f64);
/// ```

#[proc_macro_attribute]
pub fn quantity(attr: TokenStream, item: TokenStream) -> TokenStream {
    let q_attrs = parse_macro_input!(attr as QuantityAttrs);
    let input = parse_macro_input!(item as DeriveInput);

    if let Some((name, typ)) = extract_name_and_type(&input) {
        quantity_traits::expand_trait_impls(&input, q_attrs, name, typ).into()
    }
    else {
        quote!{#input}.into()
    }
}

fn extract_name_and_type(input: &DeriveInput) -> Option<(&Ident, &Type)> {
    let name = &input.ident;

    if let Data::Struct(DataStruct{ fields: Fields::Unnamed(FieldsUnnamed{ unnamed: uf, ..}), ..}) = &input.data {
        if uf.len() == 1 {
            if let Some(Field { vis: Visibility::Inherited, ty: typ, .. }) = uf.first() {
                return Some((name, typ));
            }
        }
    }
    None
}
