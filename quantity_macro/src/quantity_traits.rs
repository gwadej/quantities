//! Implementation of the Quantity derive macro.
use proc_macro2::TokenStream;
use syn::{DeriveInput,
    Expr, ExprLit,
    Ident, Token, Type,
    Meta, MetaNameValue,
    Lit, LitStr,
};
use syn::parse::{Parse, ParseStream, Result};
use syn::punctuated::Punctuated;
use quote::quote;

// Structure wrapping the arguments to the quantity attribute macro.
//
// * units - string specifying units for Display
// * alt - string specifying alternate units for Display
#[derive(Debug, Default)]
pub(crate) struct QuantityAttrs {
    units: Option<LitStr>,
    alt: Option<LitStr>,
}

impl QuantityAttrs {
    // Utility method to clean up extracting the value LitStr from the NameValue
    fn litstr_from_name_value(pair: MetaNameValue) -> Option<LitStr> {
        if let Expr::Lit(ExprLit{ lit: Lit::Str(litstr), ..}) = pair.value {
            return Some(litstr);
        }
        None
    }
}

impl Parse for QuantityAttrs {
    // Parse from a stream of tokens
    fn parse(input: ParseStream) -> Result<Self> {
        let vars = Punctuated::<Meta, Token![,]>::parse_terminated(input)?;
        let mut args = QuantityAttrs::default();
        for meta in vars.into_iter() {
            if let Meta::NameValue(pair) = meta {
                if pair.path.is_ident("units") {
                    if let Some(litstr) = Self::litstr_from_name_value(pair) {
                        args.set_units(litstr);
                    }
                    continue;
                }
                if pair.path.is_ident("alt") {
                    if let Some(litstr) = Self::litstr_from_name_value(pair) {
                        args.set_alt(litstr);
                    }
                    continue;
                }
            }
        }
        Ok(args)
    }
}

impl QuantityAttrs {
    pub fn set_units(&mut self, val: LitStr) { self.units = val.into(); }
    pub fn set_alt(&mut self, val: LitStr) { self.alt = val.into(); }

    pub fn is_empty(&self) -> bool { self.units.is_none() && self.alt.is_none() }

    pub fn units_line(&self) -> Option<TokenStream> {
        match (self.units.as_ref(), self.alt.as_ref()) {
            (None, None) => None,
            (Some(units), None) | (None, Some(units)) => Some(quote!{let units = #units;}),
            (Some(units), Some(alt)) =>
                Some(quote!{let units = if f.alternate() { #alt } else { #units };}),
        }
    }
}

pub(crate) fn expand_trait_impls(input: &DeriveInput, disp_units: QuantityAttrs, name: &Ident, typ: &Type) -> TokenStream {
    let cmp_traits = compare_traits(name);
    let ops_traits = ops_traits(name, typ);

    if disp_units.is_empty() {
        quote!{#input

// Cmp traits
#cmp_traits

// Ops traits
#ops_traits
}
    }
    else {
        let display_impl = display_trait(name, disp_units);
        quote!{#input

// Display trait
#display_impl

// Cmp traits
#cmp_traits

// Ops traits
#ops_traits
}
    }
}

// Create implementations for the [`PartialOrd`] and [`PartialEq`] traits.
fn compare_traits(q_name: &Ident) -> TokenStream {
    quote!{
#[automatically_derived]
impl<Rhs> PartialEq<Rhs> for #q_name
    where
        Rhs: Into<#q_name> + Copy
{
    fn eq(&self, other: &Rhs) -> bool {
        let rhs: Self = (*other).into();
        self.0 == rhs.0
    }
}
#[automatically_derived]
impl<Rhs> PartialOrd<Rhs> for #q_name
    where
        Rhs: Into<#q_name> + Copy
{
    fn partial_cmp(&self, other: &Rhs) -> Option<std::cmp::Ordering> {
        let rhs: Self = (*other).into();
        self.0.partial_cmp(&rhs.0)
    }
}
    }
}

// Create implementations for the [`Add`], [`Sub`], [`AddAssign`], [`SubAssign`],
// [`Mul`], [`Div`], and [`DivAssign`]
fn ops_traits(q_name: &Ident, v_type: &Type) -> TokenStream {
    quote!{
#[automatically_derived]
impl<T> std::ops::Add<T> for #q_name
    where
        T: Into<#q_name>
{
    type Output = Self;
    fn add(self, rhs: T) -> Self::Output {
        Self(self.0 + rhs.into().0)
    }
}
#[automatically_derived]
impl<T> std::ops::Sub<T> for #q_name
    where
        T: Into<#q_name>
{
    type Output = Self;
    fn sub(self, rhs: T) -> Self::Output { Self(self.0 - rhs.into().0) }
}
#[automatically_derived]
impl<T> std::ops::AddAssign<T> for #q_name
    where
        T: Into<#q_name>
{
    fn add_assign(&mut self, rhs: T) { self.0 += rhs.into().0 }
}
#[automatically_derived]
impl<T> std::ops::SubAssign<T> for #q_name
    where
        T: Into<#q_name>
{
    fn sub_assign(&mut self, rhs: T) { self.0 -= rhs.into().0 }
}
#[automatically_derived]
impl std::ops::Mul<#v_type> for #q_name {
    type Output = Self;
    fn mul(self, rhs: #v_type) -> Self::Output { Self(self.0 * rhs) }
}
#[automatically_derived]
impl std::ops::MulAssign<#v_type> for #q_name {
    fn mul_assign(&mut self, rhs: #v_type) { self.0 *= rhs }
}
#[automatically_derived]
impl std::ops::Mul<#q_name> for #v_type {
    type Output = #q_name;
    fn mul(self, rhs: #q_name) -> Self::Output { #q_name(self * rhs.0) }
}
#[automatically_derived]
impl std::ops::Div<#v_type> for #q_name {
    type Output = Self;
    fn div(self, rhs: #v_type) -> Self::Output { Self(self.0 / rhs) }
}
#[automatically_derived]
impl std::ops::DivAssign<#v_type> for #q_name {
    fn div_assign(&mut self, rhs: #v_type) { self.0 /= rhs }
}
    }
}

// Implement the [`Display`] trait.
fn display_trait(q_name: &Ident, q_attrs: QuantityAttrs) -> TokenStream {
    let unit_line = q_attrs.units_line();

    quote!{
#[automatically_derived]
impl std::fmt::Display for #q_name {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        #unit_line
        self.0.fmt(f)?;
        write!(f, " {units}")
    }
}
    }
}
