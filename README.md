# Quantities Experiment

I was looking at the NewType pattern in Rust and realized that it's a good way to implement
the `Value Object` pattern. Given Rust's strictness around types, I figured this would be a
good example to try out.

So, I implemented a number of length types to see how they would work, including conversions
between them. Next, I wanted to implement some standard arithmetic operators. With a little
help from the `Into<L>` bounds on the operators, I found that I could implement the operations
not just for the types themselves but also for any type that converts to those types with
`From`.

The only problem with this approach was a large amount of boilerplate. So, I dug into derive
proc-macros and workspaces to remove that boilerplate. Surprisingly, that worked pretty well.
